const express=require('express')
const multer = require('multer');
const mongoose=require('mongoose')
const bodyParser=require('body-parser')
const morgan=require('morgan');
const cors=require('cors')
const app=express()

//mongodb 
mongoose.connect('mongodb://localhost:27017/Videoserver',{
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true
},console.log("database connected"))

//middleware
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

app.use('/api/videos',express.static('media/uploads'))

//routes
app.use('/api/upload',require('./routes/upload'));
app.use('/api/videoList',require('./routes/videoList'));

module.exports=app