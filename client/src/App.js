import React from 'react';
import {Route} from 'react-router-dom'
import './App.css';
import Upload from './components/upload';
import Videolist from './components/videoplayer'


function App() {
  return (
    <>
    <Route exact path='/' component={Upload} />
    <Route exact path='/videolist' component={Videolist} />
    </>
  );
}

export default App;
